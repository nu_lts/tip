<?php
/**
 * @file
 * tip.features.inc
 */

/**
 * Implements hook_node_info().
 */
function tip_node_info() {
  $items = array(
    'tip' => array(
      'name' => t('Tip'),
      'base' => 'node_content',
      'description' => t('Used to create a form where the public can submit tips. When tips are approved, they are listed. Highlighted tips are displayed in the Tip of the Day widget.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
