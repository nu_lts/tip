<?php
/**
 * @file
 * tip.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function tip_taxonomy_default_vocabularies() {
  return array(
    'detailed_subjects' => array(
      'name' => 'Detailed subjects',
      'machine_name' => 'detailed_subjects',
      'description' => 'Used to identify supported subject areas for librarians, tips, digital resources',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'tip_type' => array(
      'name' => 'Tip type',
      'machine_name' => 'tip_type',
      'description' => 'Used to group tips, required for publication but not submission',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
